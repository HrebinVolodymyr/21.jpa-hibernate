package com.lviv.hv.controller;

import com.lviv.hv.model.PK_WorksOn;
import com.lviv.hv.model.WorksOnEntity;
import com.lviv.hv.service.WorkOnService;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class WorksOnController {

  @Autowired
  WorkOnService workOnService;

  @RequestMapping(value = "/worksOn", method = RequestMethod.GET)
  public String showAllWorksOn(ModelMap model) {
    try {
      model.addAttribute("table", workOnService.findAll());
    } catch (Exception e) {
      e.printStackTrace();
    }
    return "works";
  }

  @RequestMapping(value = "/worksOnFindById", method = RequestMethod.GET)
  public String findById(
      @RequestParam("id") String id,
      ModelMap model) {
    try {
      if (id != null) {
        List<WorksOnEntity> list = new ArrayList<WorksOnEntity>();
        for (WorksOnEntity worksOnEntity : workOnService.findAll()) {
          Integer id_int = null;
          try {
            id_int = Integer.parseInt(id);
          } catch (Exception e) {
          }
          if (worksOnEntity.getProjectNo().equals(id)
              || worksOnEntity.getEmpNo().equals(id_int)) {
            list.add(worksOnEntity);
            System.out.println("error" + worksOnEntity.getDate());
            model.addAttribute("table", list);
          }
        }
        return "works";
      }
      model.addAttribute("table", workOnService.findAll());
    } catch (Exception e) {
      e.printStackTrace();
    }
    return "works";
  }

  @RequestMapping(value = "/worksOn/delete/{id_emp}/{id_project}", method = RequestMethod.GET)
  public String deleteEmployee(@PathVariable("id_emp") Integer id_emp,
      @PathVariable("id_project") String id_project, ModelMap model) {
    PK_WorksOn pk = new PK_WorksOn(id_emp, id_project);
    try {
      workOnService.delete(pk);
      model.addAttribute("table", workOnService.findAll());
    } catch (Exception e) {
      try {
        model.addAttribute("table", workOnService.findAll());
        model.addAttribute("error", "ERROR: " + ExceptionUtils.getRootCause(e).getMessage());
      } catch (Exception s) {
        s.printStackTrace();
      }
    }
    return "works";
  }

  @RequestMapping(value = "/worksOn/add", method = RequestMethod.GET)
  public String add(ModelMap model) {
    return "addWorkOn";
  }

  @RequestMapping(value = "/worksOn/add", method = RequestMethod.POST)
  public String addDepartment(
      @RequestParam("empNo") Integer empNo,
      @RequestParam("projectNo") String projectNo,
      @RequestParam("job") String job,
      @RequestParam("date") Date date,
      ModelMap model) {
    WorksOnEntity entity = new WorksOnEntity(empNo, projectNo, job, date);
    try {
      workOnService.create(entity);
    } catch (Exception e) {
      model.addAttribute("error", "ERROR: " + ExceptionUtils.getRootCause(e).getMessage());
      return "addWorkOn";
    }
    return "redirect:/worksOn";
  }

  @RequestMapping(value = "/worksOn/edit/{id_emp}/{id_project}", method = RequestMethod.GET)
  public String edit(@PathVariable("id_emp") Integer id_emp,
      @PathVariable("id_project") String id_project, ModelMap model) {
    try {
      List<WorksOnEntity> list = workOnService.findAll();
      for (WorksOnEntity worksOnEntity : list) {
        if (worksOnEntity.getEmpNo().equals(id_emp)
            && worksOnEntity.getProjectNo().equals(id_project)) {
          System.out.println("error" + worksOnEntity);
          model.addAttribute("entity", worksOnEntity);
        }
      }
    } catch (Exception e) {
      model.addAttribute("error", "ERROR: " + ExceptionUtils.getRootCause(e).getMessage());
      System.out.println();
      return "editWorkOn";
    }
    return "editWorkOn";
  }

  @RequestMapping(value = "/worksOn/edit/update", method = RequestMethod.POST)
  public String update(
      @RequestParam("empNo") Integer empNo,
      @RequestParam("projectNo") String projectNo,
      @RequestParam("job") String job,
      @RequestParam("date") Date date,
      ModelMap model) {
    WorksOnEntity entity = new WorksOnEntity(empNo, projectNo, job, date);
    try {
      workOnService.update(entity);
    } catch (Exception e) {
      model.addAttribute("error", "ERROR: " + ExceptionUtils.getRootCause(e).getMessage());
      return "redirect:/worksOn";
    }
    return "redirect:/worksOn";
  }

}
