package com.lviv.hv.controller;

import com.lviv.hv.model.ProjectEntity;
import com.lviv.hv.service.ProjectService;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder.In;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ProjectController {

  @Autowired
  ProjectService projectService;

  @RequestMapping(value = "/project", method = RequestMethod.GET)
  public String showProjects(ModelMap model) {
    try {
      model.addAttribute("table", projectService.findAll());
    } catch (Exception e) {
      e.printStackTrace();
    }
    return "projects";
  }

  @RequestMapping(value = "/projectFindById", method = RequestMethod.GET)
  public String findById(
      @RequestParam("id") String id,
      ModelMap model) {
    try {
      if (id != null) {
        List<ProjectEntity> list = new ArrayList<ProjectEntity>();
        list.add(projectService.findById(id));
        model.addAttribute("table", list);
        return "projects";
      }
      model.addAttribute("table", projectService.findAll());
    } catch (Exception e) {
      e.printStackTrace();
    }
    return "projects";
  }

  @RequestMapping(value = "/project/delete/{id}", method = RequestMethod.GET)
  public String deleteProject(@PathVariable("id") String projectId, ModelMap model) {
    try {
      projectService.delete(projectId);
      model.addAttribute("table", projectService.findAll());
    } catch (Exception e) {
      try {
        model.addAttribute("table", projectService.findAll());
        model.addAttribute("error", "ERROR: " + ExceptionUtils.getRootCause(e).getMessage());
      } catch (Exception s) {
        s.printStackTrace();
      }
    }
    return "projects";
  }

  @RequestMapping(value = "/project/add", method = RequestMethod.GET)
  public String add(ModelMap model) {
    return "addProject";
  }

  @RequestMapping(value = "/project/add", method = RequestMethod.POST)
  public String addProject(
      @RequestParam("projectNo") String projectNo,
      @RequestParam("projectName") String projectName,
      @RequestParam("budget") String budget,
      ModelMap model) {
    try {
      Integer budgetInt = Integer.parseInt(budget);
      ProjectEntity entity = new ProjectEntity(projectNo, projectName, budgetInt);
      projectService.create(entity);
    } catch (Exception e) {
      model.addAttribute("error", "ERROR: " + ExceptionUtils.getRootCause(e).getMessage());
      return "addProject";
    }
    return "redirect:/project";
  }

  @RequestMapping(value = "/project/edit/{id}", method = RequestMethod.GET)
  public String edit(@PathVariable("id") String projectId, ModelMap model) {
    try {
      model.addAttribute("entity", projectService.findById(projectId));
    } catch (Exception e) {
      model.addAttribute("error", "ERROR: " + ExceptionUtils.getRootCause(e).getMessage());
      return "projects";
    }
    return "editProject";
  }

  @RequestMapping(value = "/project/edit/update", method = RequestMethod.POST)
  public String updateProject(
      @RequestParam("projectNo") String projectNo,
      @RequestParam("projectName") String projectName,
      @RequestParam("budget") String budget,
      ModelMap model) {
    try {
      Integer budgetInt = Integer.parseInt(budget);
      ProjectEntity entity = new ProjectEntity(projectNo, projectName, budgetInt);
      projectService.update(entity);
    } catch (Exception e) {
      model.addAttribute("entity", projectService.findById(projectNo));
      model.addAttribute("error", "ERROR: " + e.getMessage());
      return "editProject";
    }
    return "redirect:/project";
  }


}
