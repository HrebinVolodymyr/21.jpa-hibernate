package com.lviv.hv.service;

import com.lviv.hv.model.ProjectEntity;

public interface ProjectService extends GeneralService<ProjectEntity, String> {

}
