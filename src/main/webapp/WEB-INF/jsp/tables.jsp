<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Title</title>
</head>
<body>

<c:forEach items="${tables}" var="entry">
    <c:set var="table" value="${entry.value}" scope="request"/>
    <c:if test="${entry.key eq 'employees'}">
        <jsp:include page="employees.jsp"/>
    </c:if>
    <c:if test="${entry.key eq 'departments'}">
        <jsp:include page="departments.jsp"/>
    </c:if>
    <c:if test="${entry.key eq 'projects'}">
        <jsp:include page="projects.jsp"/>
    </c:if>
    <c:if test="${entry.key eq 'works'}">
        <jsp:include page="works.jsp"/>
    </c:if>
</c:forEach>

</body>
</html>
